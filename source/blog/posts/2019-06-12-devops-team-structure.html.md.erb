---
title: "The ideal DevOps team structure"
author: Chrissie Buchanan
author_gitlab: cbuchanan
author_twitter: gitlab
categories: insights
image_title: '/images/blogimages/devops-team-structure.jpg'
description: "Dev and Ops working together is a beautiful thing. What is the ideal structure for DevOps to thrive?"
tags: DevOps, agile
cta_button_text: 'Just commit'
cta_button_link: '/just-commit/application-modernization/'
twitter_text: "What is the ideal #DevOps team structure?"
postType: content marketing
---

The seamless collaboration between Development and IT operations is a beautiful thing. DevOps was designed to remove silos so that these teams could work together to build, test, and deploy software faster. But there’s a lot more to DevOps than just a philosophy and a catchy abbreviation – the structure component goes much deeper than that.

For those that may be unfamiliar with [Conway’s Law](http://www.melconway.com/Home/Conways_Law.html), it goes something like this:

>“Any organization that designs a system (defined broadly) will produce a design whose structure is a copy of the organization's communication structure.”

So what is the ideal DevOps team structure? There are a few things to consider here. The only way to know if your existing structure works is if you _feel_ like it works – Dev and Ops are working together and business objectives are being met or exceeded. What that looks like for every company is a little bit different and it helps to analyze different models. By looking at the pros and cons of each, and considering Conway’s Law, you can find a better fit for your team’s unique needs.

Several factors come into play when it comes to team structure:


*   **Existing silos**: Are there product sets/teams that work independently?
*   **Technical leadership**: Who leads teams and what is their industry experience? Do Dev and Ops have the same goals or are they guided by the individual experience of their leaders?
*   **IT Operations**: Has operations fully aligned with the goals of the business, or are they still just seen as configuring servers and assisting the development team with their agenda?
*   **Knowledge gaps**: Is the organization equipped today with the skills and human resources to change the DevOps structure?


## Making sense of silos

[Matthew Skelton’s blog](https://blog.matthewskelton.net/2013/10/22/what-team-structure-is-right-for-devops-to-flourish/) covers a number of different DevOps scenarios in great detail, but we’ll discuss just a few of the silos he mentions specifically and how they impact an organization.


### Dev and Ops are completely separate

Skelton refers to this as a classic “throw it over the wall” team structure and, as implied, it’s not the most effective DevOps strategy. Both teams work in their bubbles and lack visibility into the workflow of the other team. This complete separation lacks collaboration, visibility, and understanding – vital components of what effective DevOps _should_ be. What happens is essentially blame-shifting: "**_We_** don’t know what **_they_** are doing over there, **_we_** did our part and now it's up to **_them_** to complete it," and so on. Do you see the pattern?

Going back to Conway’s Law, this is an organization that clearly doesn’t communicate well, so they have created a structure that reflects this probably without realizing it. This is not great DevOps.


### DevOps middleman

In this team structure, there are still separate Dev and Ops teams, but there is now a “DevOps” team that sits between, as a facilitator of sorts. This is not necessarily a bad thing and Skelton stresses that this arrangement has some use cases. For example, if this is a temporary solution with the goal being to make Dev and Ops more cohesive in the future, it could be a good interim strategy.


### Ops stands alone

In this scenario, Dev and DevOps are melded together while Ops remains siloed. Organizations like this still see Ops as something that supports the initiatives for software development, not something with value in itself. Organizations like this suffer from basic operational mistakes and could be much more successful if they understand the value Ops brings to the table.


## The importance of leadership

Let’s assume that, in your organization, the dysfunction you experience is 100% confirmed by Conway’s Law and there will need to be a major shift in communication in order to improve your DevOps structure. How can you do that? The secret to overcoming the challenges of cultural change related to DevOps implementations can be found in the way leaders _lead_.

Organizational change initiatives are notoriously difficult: There has to be a company-wide buy-in and many departments will have to agree on a course of action. Change isn’t easy even the most ideal scenarios, let alone organizations that aren’t communicating as well in the first place. Some of the biggest predictors of failure are:


*   Resistance to change
*   Low readiness for change
*   Poor employee engagement

[Transformational leadership](https://techbeacon.com/devops/transformational-leadership-what-every-devops-leader-needs-know) has a direct influence on how team members respond to DevOps changes in processes, technology, roles, and mindsets.

When it comes to defining specific roles and their functions, the team at Puppet [made these recommendations](https://puppet.com/blog/what%E2%80%99s-best-team-structure-for-devops-success):



*   **IT manager**: Builds trust with counterparts on other teams; creates a climate of learning and continuous improvement; delegates authority to team members
*   **Dev manager**: Builds trust with Ops; bring Ops into the planning process early
*   **Systems engineer**: Automates the things that are painful
*   **Quality engineer**: Provides input into scale, performance, and on staging environments
*   **Devs**: Plans deployments of new features with feedback from Ops and works with them on deployment processes


## Getting Ops involved

Operations is a discipline with its own methodologies. Just because modern cloud hosting makes it easier than ever to deploy servers without having to know one end of a SCSI cable from another doesn’t mean that everyone is an Ops master. What Ops brings to the SDLC is reliability, performance, and stability. Devs can help the production environment by using their skills to automate processes, and true DevOps plays to the strengths of each.

DevOps does not mean that developers manage production.

Dev and Ops can be in direct conflict with each other because both teams are incentivized in vastly different ways: Operations emphasizing availability, Development emphasizing feature delivery. Availability requires caution while caution is the very antithesis of speed, but both teams can learn from each other and benefit from their experience.

Ops is an ally, not a barrier, in the SDLC.


## Mind the gap(s)

What would you need today to create a more efficient DevOps team structure? Going back to Conway’s Law, it’s important to analyze how your team communicates now and think objectively about what should be better and what you would like to create. Tools can’t solve cultural problems.

Organizations have embraced new structures in order to achieve certain outcomes, and they understand the link between organizational structure and the software they create. For example, Netflix and Amazon [structure themselves around multiple small teams](https://www.thoughtworks.com/insights/blog/demystifying-conways-law), each one autonomous over a small part of the system. Teams with more monolithic codebases just can’t work this way, and in order to adopt the Netflix DevOps model, they would need to adopt a microservices architecture as well.

Microservices and containers enable a DevOps model that iterates quickly and offers more autonomy within certain groups. The architecture of the code environment has a large effect on how teams work together.

Since GitLab is a complete DevOps platform, delivered as a single application, our Dev teams are organized into stages (e.g. [Verify](/handbook/engineering/ops/verify/) group, [Create](/handbook/engineering/dev-backend/create/) group, etc.) because these would be separate products at any other company and require their own autonomy. We also have other functional DevOps groups besides "Dev" that manage other aspects of our product. We have an SRE team that manages uptime and reliability for GitLab.com, a [Quality department](/handbook/engineering/quality/),  and a [Distribution team](/handbook/engineering/development/enablement/distribution/), just to name a few. The way that we make all these pieces fit together is through [our commitment to transparency](/blog/2017/03/14/buffer-and-gitlab-ceos-talk-transparency/) and our visibility through the entire SDLC. We’re also dedicated to [cloud native development](/blog/2017/11/30/containers-kubernetes-basics/) through containers and Kubernetes, which enables us to release faster.

A team structure that facilitates collaboration and visibility between the Dev and Ops teams, as well as tools that automate processes, are the hallmarks of an ideal DevOps lifecycle. Keep in mind that good DevOps _doesn’t_ mean that everybody does everybody’s job. [Should developers do Ops](/blog/2019/06/05/modernize-your-ci-cd/)? Not necessarily.

Many in the beginning thought the goal of DevOps was to combine the Dev, QA, and Ops departments into a single team: Have everyone do everything and – boom – instant innovation. [These strategies, unsurprisingly, failed](https://hackernoon.com/devops-team-roles-and-responsibilities-6571cfb56843).

Specialists can add value, but a lack of cohesion between the Dev and Ops processes leads to unnecessary dysfunction over time. It should not be **_we_** and **_them_** – it should be **_us_**. An organization that communicates like this will inevitably build a structure that operates in much the same way. The ideal DevOps team structure is the one that lets teams work together effectively and removes the barriers between code and production.

Are you ready to build a better DevOps structure? [Just commit](/just-commit/application-modernization/).

Photo by [Zbysiu Rodak](https://unsplash.com/@zbigniew?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/search/photos/blocks?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
{: .note}
