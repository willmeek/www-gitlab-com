---
layout: markdown_page
title: "FedRAMP and GitLab"
---

## What is FedRAMP?

The Federal Risk and Authorization Management Program (FedRAMP) is a government-wide program that provides a standardized approach to security assessment, authorization, and continuous monitoring for cloud products and services. [See the GSA definition.](https://www.gsa.gov/technology/government-it-initiatives/fedramp)

Specifically, FedRAMP is focused on cloud products and services, evaluating and certifying cloud PAAS, IAAS, and SAAS offerings.   Details about the [FedRAMP program](https://www.fedramp.gov/) highlight the process and status of how cloud services are assessed and certified in the [FedRAMP marketplace.](https://marketplace.fedramp.gov/#/products?sort=productName)

## GitLab and FedRAMP

GitLab is **both** a product that you can host and a cloud service that we host.

You can deploy GitLab into your FedRAMP cloud PaaS such as [AWS](https://docs.gitlab.com/ee/install/aws/), [Google Cloud](https://docs.gitlab.com/ee/install/google_cloud_platform/index.html), [Azure](https://docs.gitlab.com/ee/install/azure/index.html), and others.  In fact, many agencies and customers deploy and manage their own instance of GitLab in their cloud with the majority of our government customers using an on-premise instance of our software product in their cloud environment (like AWS, Google Cloud, or Azure) or in a hybrid or multi-cloud environment. Other customers find that they can install and run a self-managed instance of the GitLab product by placing it in a FedRAMP-authorized data center and using a FedRAMP-authorized Cloud Service Provider.

For our hosted offering of GitLab.com, we're working toward FedRAMP certification. When we have an expected timeline for achieving FedRAMP-authorized status, we will add it to our product roadmap.

If you want to learn more about GitLab and how we support public sector agencies, departments, and organizations, please contact us.

<center><a href="/solutions/public-sector/#contact" class="btn cta-btn orange">contact our public sector team</a></center>
