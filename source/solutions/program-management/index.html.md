---
layout: markdown_page
title: "Program Management"
---
## Manage Programs Across Multiple Projects and Groups
GitLab provides high-level visibility into and control over the people and projects aligned with ongoing business initiatives.  Program Managers can define and enforce policy and permissions, track progress and velocity across multiple projects and groups, and prioritize initiatives to deliver the greatest amount of value.

## Program Creation and Governance

[Subgroups](https://docs.gitlab.com/ee/user/group/subgroups/#subgroups) allow administrators to organize related projects and teams into programs while retaining granular security, visibility, and access for each project or group of users.
<br> <br>
[Project Templates](https://docs.gitlab.com/ee/user/group/custom_project_templates.html) provide efficiency and consistency, ensuring project managers define projects that meet minimum criteria and align with larger program-level goals.

## Program Consistency and Planning

With [Multi-level Epics](https://docs.gitlab.com/ee/user/group/epics/), Program Managers can group multiple initiatives into a single program, gaining high-level 
visibility across a broad scope of resources, but still maintaining the ability to drill down into the most tactical details of a project or issue. Epics can be 
managed by group or in aggregate at the top level, to help manage larger collections of work.
<br> <br>
[Roadmaps](https://docs.gitlab.com/ee/user/group/roadmap/) enable businesses to track current effort in flight and plan upcoming work to best utilize their 
resources and focus on the right priorities. Users can select timescales that matter most for their planning initiatives, whether that’s a two-week sprint or 
quarterly or annual planning. Like Epics, Roadmaps can be managed by group or in aggregate.

## Program Analytics

[Value Stream Analytics](https://docs.gitlab.com/ee/user/analytics/value_stream_analytics.html) tracks the time spent to go from an idea to production for each of your projects, so you can identify opportunities for improvement and target resources appropriately. 
Value Stream Analytics displays the median time spent in each stage defined in the process.
<br> <br>
[Group Insights](https://docs.gitlab.com/ee/user/group/insights/) allows users to visualize different trends in workflows, bugs, merge request (MR) throughput, and issue activity that is based upon a group’s underlying [labeling](https://docs.gitlab.com/ee/user/project/labels.html#overview) system. Users can further customize Insights reports by editing a [simple YML config file]( https://docs.gitlab.com/ee/user/project/insights/index.html).
<br> <br>
[Productivity Analytics](https://docs.gitlab.com/ee/user/analytics/productivity_analytics.html) tracks development velocity across projects, providing an estimate of how long, on average, it takes to deliver features.
<br><br>
[Contribution Analytics](https://docs.gitlab.com/ee/user/group/contribution_analytics/) offers a view into team contributions, allowing Program Managers to identify high or low performing teams and developers for rewards or assistance.
