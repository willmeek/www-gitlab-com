---
layout: handbook-page-toc
title: How to use mustread
category: General
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

##### Overview

@must-read transforms any important message into micro-task. So, no one will miss it. It is a bot that will helps our team to collect important messages, announcements and links, and track who have read it.
@must-read helps to control all significant information no matter how many channels you have and how big is your team. You can easily check who has read the message and who has not. No more “Did you read it guys?” questions needed.

---

##### Use cases
- Use it to send short important announcements for whole team or channel
- Use it when your message is too small to become a task/card/issue but too important to be forgotten/lost/ignored
- Bookmark important links for yourself or any member of your team
- Send links to important news, tasks, meetings or support tickets
- Use must-read messages as simple reminders
- When you got notifications from Zendesk, Trello, Asana, JIRA, etc. that need more attention, simply        start a thread and mention `@must-read`

---
##### Workflow
1. Here are some simple commands that you can 
   - Mark any messages to track reactions of the team:
      - Mention `@mustread` and teammates (who must read) if you want to get their reaction.
        Also you can use @channel or @here. I'll collect all these messages in follow-up list. 
        E.g. `@must-read: example.net @user1 @user2`
        I'll add ✅ to this message. If somebody clicks on this reaction, it will mean they read it. When everyone has reacted you will see 📙. If you mention only `@must-read` in a message it will be must-read for everyone in the channel.

   - Stealth mode:
      - If you add nobody but `@mustread` to any link (or 70+ chars of important text for your teammates)    it will be must-read for everyone in the channel.
        E.g. `@must-read: example.net`
        or `@must-read: some important text for a channel 70 or more characters long`

---

Please refer the [help page](https://finalem.com/must-read/help) for more information on different use cases.
