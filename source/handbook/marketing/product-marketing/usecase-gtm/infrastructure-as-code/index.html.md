---
layout: markdown_page
title: "Usecase: Infrastructure as Code / GitOps"
---

<!--

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
-->

## Usecase

Define and describe the usecase in customer terms.  Reference the [customer usecase page](/use-cases)


## Personas

### User Persona
Who are the users (a few remarks and link to the persona page)

### Buyer Personas
Who are the buyers

## [Message House](./message-house/)

The message house for the use case provides a structure to describe and discuss the value and differentiators for the use case.

### Discovery Questions
- list key discovery questions

## Analyst Coverage

List key analyst coverage of this usecase

### [AR Plan](./ar-plan/)

  The AR Plan provides key details into how we intend to engage with the analyst community.

## UseCase Capabilities

| Capability |  Description  |  Typical features  |  Value |
|----------|-------------|----------------|-------|
| abc  |  def  |  ghi  |  jkl  |  

## Top 3 Differentiators

| Differentiator | Value | Proof Point  |
|-----------------|-------------|---------------|
|  abc  | def | ghi  |


## Competitive Comparison
TBD - will be a comparison grid leveraging the capabilities

## Proof Points - customers

### Quotes and reviews
- List of customer quotes/reviews from public sites

### Case Studies
- List of case studies

### References to help you close
- Link to SFDC list of use case specific references

## Partners
- Describe how key partners help enable this usecase

## Resources
### Presentations
* LINK

### Whitepapers and infographics
* LINK

### Videos (including basic demo videos)
* LINK

### Integrations Demo Videos
* LINK

### Clickthrough & Live Demos
* Link
