---
layout: handbook-page-toc
title: "Business Systems: Portal Analysis, Integrations, and Flow"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Contact
[Business Systems Analyst \| Jamie Carey](/job-families/finance/business-system-analyst/)

## Issues
* [gitlab-com](https://gitlab.com/groups/gitlab-com/-/boards/1553398?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=EntApp%20Ecosystem)
* [gitlab-org](https://gitlab.com/groups/gitlab-org/-/boards/1553395?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=EntApp%20Ecosystem)

## Defined Technology Approach
##### Sales
Directly Responsible Individual James Harrison
- Create a clear, consistent workflow for Sales to be trained on new business and renewals.
- Create a clear, consistent process on how to get self-service support or Sales support (previously the SMB customer advocate function played this role) and document in handbook.
- Solve: Salesforce to Zendesk synchronization problems.

##### Product
Directly Responsible Individual Michael Karampalas/Amanda Rueda
- Customers and Sales need to be provided with a place to look in an easy to read format of “known bugs” that are affecting the portal, what is the workaround, how to ask for the workaround to be applied to them, and what the timeline to fix is.
This should be created by Product and maintained by both Product and Support.
- Product has been working on improvements to the portal’s user interface, user experience, and backend processes of the portal.
There’s identifiable work to execute as soon as possible and over the next year.
- Retention:
   - .com auto renew at right amount of seats
   - .com toggle a/b test
   - Correctly downgrade .com that fail to renew
   - True up screen context in portal
   - Renewal banner updates
   - .com proration
   - Cloud license sync
   - Security issues
- Fulfillment:
   - Finish SOAP to REST
   - Pajamas (although with this stretching we should discuss how we want to approach finishing)
   - .com proration (MVC messaging, API, prorated charges)
   - Cloud license sync
- True ups, add-ons: simplify for customer.
     - Charge customers properly when they add new users to a group on GitLab.com
     - Provide more context and guidance on true-up screen for self-managed customers
     - Transparently document GitLab's renewal and true-up process
     - GitLab Cloud Activation & Sync
- Sales should know what a customer’s user counts are for the next renewal.
     - Push true-up data, auto-renew status, and DPE to SalesForce
- The ability for customers to self-serve the purchase of users between renewals.
     - Charge customers properly when they add new users to a group on GitLab.com
- Engineers will provide “support” by manually providing solutions for Sales such as sending licenses that weren’t sent.
- Consolidate customer portal, license, and version applications into a single application.
- Evaluate how the product could lead customers through renewals for self-managed (eg wizard).
- Evaluate in product navigation with account management tools.
     - Part 1: UX Scorecard for Growth: Renew My Account
- It’s recommended that Product additionally provide regular and clear updates to the Sales and Customer Success organizations about work in progress and upcoming work that has an impact on those customer facing organizations.

##### Support
Directly Responsible Individual Jason Coyler
- Support, triage and work the Zendesk License and Renewal queue.
- Provide feedback to teams that intersect with the portal.
- Document known bugs and workarounds in appropriate locations as identified by Product.
- Participate in single lane channel identified by Sales to request help from Support.
- Work with Sales Ops to resolve Salesforce synchronization problems.
- Create: “Support” contacts on accounts.

##### Business Operations
Directly Responsible Individual Jamie Carey
- With Business System Specialist, provide support to Sales, Support, and Product in creating interdepartmental feedback loops, workflows, and processes.
- Continue to monitor and illuminate gaps.
- Document when EULAs are required before a license can be issued.
- Document the relationship between the customer portal and the product for Docs and Sales to provide to the customer during onboarding.
- Work with Product, Sales, and Support on a unified list of product features and functionality in prioritized order for Product.
- Update all customer facing troubleshooting and how-to documentation on new business and renewals on how to contact Sales if desired until it’s consolidated.
- Solve: multiple billing contacts
- Work with Billing on adding dunning features into Zuora (ie notices to customer on expired credit cards on file).

## High Level Setup
The customer portal is a GitLab created and managed application: there are three related applications, customers.gitlab.com, version.gitlab.com, and license.gitlab.com
Additionally, there are three other points of sale online: via the [account's billing area in a .com instance](https://gitlab.com/profile/billings), [AWS marketplace](https://aws.amazon.com/marketplace/seller-profile?id=9657c703-ca56-4b54-b029-9ded0fadd970, and [GOV.UK Digital Marketplace](https://www.digitalmarketplace.service.gov.uk/g-cloud/services/526446644571793).

##### More Documentation

*  [video of custom setup](https://drive.google.com/drive/folders/1kfCEQM6XYGWYxq3Ke4TNvtmDR-46erVD)
*  [Security's Compliance Diagram](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/PCI/customers.gitlab.com_data_flow_diagram_-_New_Business.pdf)
*  [Growth Team's Portal Diagram](https://app.mural.co/t/gitlab2474/m/gitlab2474/1569500330861/8f9fd73826c42ad809d51be886db27494da91353)
*  [.com renewal flow Diagram](https://whimsical.com/S4r7nuRFgQ5kZKaZisZcoQ)
*  [Trade Compliance](/handbook/business-ops/trade-compliance/)
*  [Sales flow](https://drive.google.com/file/d/1nkJrsXewy1G9llV9-8k2EhineU2hyoDJ/view?usp=sharing)

### Customer Flow

<div class="center">
<div style="width: 640px; height: 480px; margin: 10px; position: relative;"><iframe allowfullscreen frameborder="0" style="width:640px; height:480px" src="https://www.lucidchart.com/documents/embeddedchart/d5593761-efdf-43a0-bfe5-717d787404d7" id="_OtpjB8oRZGk"></iframe></div>
</div>

### System Integrations

Important to know

* [READ ME customer portal repo](https://gitlab.com/gitlab-org/customers-gitlab-com#subscription-portal-app)
* API should send country codes ISO 2 from portal to SFDC and Marketo

<div class="center">
<div style="width: 640px; height: 480px; margin: 10px; position: relative;"><iframe allowfullscreen frameborder="0" style="width:640px; height:480px" src="https://www.lucidchart.com/documents/embeddedchart/b5a0e4b3-ff40-4685-8d21-3109696c8f64" id="0.tpYZzq.lbI"></iframe></div>
</div>

### Zuora and Salesforce

<img src="/handbook/business-ops/images/zuoraSFDC1.png" class="full-width">

##### Zuora details
* Communication Profiles: 
    * New accounts are set to `Default Profile`
    * `Silent Profile` is used temporarily when the customer should not receive notifications

## Change Management
##### Adding new SKUs
* When a new SKU is added, the Fulfillment team [must be notified thirty (30) days in advance](https://gitlab.com/gitlab-org/customers-gitlab-com/issues) with the requirements so that they can prepare the customer applications.
* When the SFDC sandbox is refreshed, the Fulfillment team is notified and the SFDC sandbox is reintegrated with the Zuora sandbox.

## Definitions of users
##### Self-managed EE
- a license holder is someone who pays for a GitLab license and manages the payments for a self-managed installation. [EULA](https://customers.gitlab.com/admin/eula), [TOU](https://about.gitlab.com/terms), [FOSS LICENSE](https://gitlab.com/gitlab-org/gitlab/blob/master/LICENSE), [EE LICENSE](https://gitlab.com/gitlab-org/gitlab/blob/master/ee/LICENSE)
- a self-managed admin is someone who manages the installation and users of a self-managed installation. [FOSS LICENSE](https://gitlab.com/gitlab-org/gitlab/blob/master/LICENSE), https://gitlab.com/gitlab-org/gitlab/blob/master/ee/LICENSE
- a self-managed user is someone who uses a self-managed installation. [FOSS LICENSE](https://gitlab.com/gitlab-org/gitlab/blob/master/LICENSE), https://gitlab.com/gitlab-org/gitlab/blob/master/ee/LICENSE

##### Dotcom
- a dotcom subscriber is someone who pays for dotcom. [EULA](https://customers.gitlab.com/admin/eula), [TOU](https://about.gitlab.com/terms)
- a dotcom user are people who can be assigned to seats that a dotcom subscriber is paying for. [TOU](https://about.gitlab.com/terms)

##### Self-managed FOSS
- a self-managed admin is someone who manages the installation and users of a self-managed installation. [FOSS LICENSE](https://gitlab.com/gitlab-org/gitlab/blob/master/LICENSE)
- a self-managed user is someone who uses a self-managed installation. [FOSS LICENSE](https://gitlab.com/gitlab-org/gitlab/blob/master/LICENSE)

## Related Documentation and Links: Licensing, Billing, Transactions
* [Customer Portal: General Issues](https://gitlab.com/gitlab-com/business-ops/bizops-bsa/general-analysis/issues/8)
* [Issue: Analysis: Licenses & Terms](https://gitlab.com/gitlab-com/business-ops/bizops-bsa/general-analysis/issues/6)
* [EULAs/TOS: As-Is + To-Be](https://gitlab.com/gitlab-com/business-ops/bizops-bsa/general-analysis/issues/3)
* [Licenses/Self Managed: As-Is + To-Be](https://gitlab.com/gitlab-com/business-ops/bizops-bsa/general-analysis/issues/4)
* [Troubleshooting: True Ups, Licenses + EULAs](/handbook/business-ops/business_systems/portal/troubleshooting/)
* [Troubleshooting subscription and licensing problems](/handbook/support/workflows/license_troubleshooting.html)
* [License documentation](https://docs.gitlab.com/ee/user/admin_area/license.html)
* [Customer Portal: Admin internal documentation](/handbook/internal-docs/customers-admin/)
* [Epic: Analysis: License and Billing Queue](https://gitlab.com/groups/gitlab-com/business-ops/bizops-bsa/-/epics/6)

## Licensing and Renewals (L&R) Queue in Zendesk
The customer portal intersects with Support, Billing, Sales, and Product primarily.
Tickets come into the L&R queue in Zendesk generally three ways, generally prioritized as low:
   - by emailing renewals@gitlab.com
   - by [opening a support ticket](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=334487) and selecting `Help with my license`
   - by filling out the form on the [Renewals page](https://about.gitlab.com/renewals/)
The L&R Queue is a separate queue of Zendesk tickets that fall outside of the scope of customer product support; it's handled by Support by triaging the issue and rerouting the tickets to the appropriate team.
   * **Passing to Sales:** Pass tickets relating to IACV to Sales via cc to Sales in reply to ticket which sends an email to both customer and account owner. If an account is associated with the ticket, there's a ticket object in SFDC.
It's then Sales's responsibility to continue the customer interaction and the BSS closes the Zendesk ticket.
   * **Passing to Sales:** If a ticket comes in and Support is unable to determine the account owner or the inquiry is related to new business, they reach out to the SFDC chatter group `ZD-newbusiness` where the SDR Regional leads for EMEA, APAC and AMER/LATAM provide an answer.
Then Support cc's that person on the ticket and closes in Zendesk.
      * Regional Leads for SDRs: 
         *  AMER/LATAM - Mona Elliot
         *  APAC - Jay Thomas-Burrows
         *  EMEA - Elsje Smart
* **Passing to Billing:** The mechanism to pass tickets to the Billing team is to open the ticket, then change the field `form` from the value `Upgrades, Renewals & AR (refunds)` to `Accounts Receivable/Refunds`.
This puts the ticket into the Billing Team's queue which they then manage.
