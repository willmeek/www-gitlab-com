---
layout: markdown_page
title: "Identity data"
---

#### GitLab Identity Data

Data as of 2020-01-31

##### Country Specific Data

| **Country Information**                     | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Based in APAC                               | 87        | 7.53%           |
| Based in EMEA                               | 328       | 28.37%          |
| Based in LATAM                              | 15        | 1.30%           |
| Based in NORAM                              | 726       | 62.80%          |
| **Total Team Members**                      | **1,156** | **100%**        |

##### Gender Data

| **Gender (All)**                            | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men                                         | 819       | 70.85%          |
| Women                                       | 335       | 28.98%          |
| Other Gender Identities                     | 2         | 0.17%           |
| **Total Team Members**                      | **1,156** | **100%**        |

| **Gender in Leadership**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Leadership                           | 56        | 78.87%          |
| Women in Leadership                         | 15        | 21.13%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **71**    | **100%**        |

| **Gender in Development**                   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Development                          | 400       | 82.82%          |
| Women in Development                        | 82        | 16.98%          |
| Other Gender Identities                     | 1         | 0.21%           |
| **Total Team Members**                      | **483**   | **100%**        |

##### Race/Ethnicity Data

| **Race/Ethnicity (US Only)**                | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 2         | 0.29%           |
| Asian                                       | 45        | 6.57%           |
| Black or African American                   | 22        | 3.21%           |
| Hispanic or Latino                          | 37        | 5.40%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 26        | 3.80%           |
| White                                       | 408       | 59.56%          |
| Unreported                                  | 145       | 21.17%          |
| **Total Team Members**                      | **685**   | **100%**        |

| **Race/Ethnicity in Development (US Only)** | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 19        | 9.69%           |
| Black or African American                   | 3         | 1.53%           |
| Hispanic or Latino                          | 9         | 4.59%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 7         | 3.57%           |
| White                                       | 123       | 62.76%          |
| Unreported                                  | 35        | 17.86%          |
| **Total Team Members**                      | **196**   | **100%**        |

| **Race/Ethnicity in Leadership (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 8         | 14.04%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 0         | 0.00%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 2         | 3.51%           |
| White                                       | 35        | 61.40%          |
| Unreported                                  | 12        | 21.05%          |
| **Total Team Members**                      | **57**    | **100%**        |

| **Race/Ethnicity (Global)**                 | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 2         | 0.17%           |
| Asian                                       | 103       | 8.91%           |
| Black or African American                   | 28        | 2.42%           |
| Hispanic or Latino                          | 56        | 4.84%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 36        | 3.11%           |
| White                                       | 640       | 55.36%          |
| Unreported                                  | 291       | 25.17%          |
| **Total Team Members**                      | **1,156** | **100%**        |

| **Race/Ethnicity in Development (Global)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 50        | 10.35%          |
| Black or African American                   | 5         | 1.04%           |
| Hispanic or Latino                          | 24        | 4.97%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 14        | 2.90%           |
| White                                       | 270       | 55.90%          |
| Unreported                                  | 120       | 24.84%          |
| **Total Team Members**                      | **483**   | **100%**        |

| **Race/Ethnicity in Leadership (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 8         | 11.27%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 1         | 1.41%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 2         | 2.82%           |
| White                                       | 41        | 57.75%          |
| Unreported                                  | 19        | 26.76%          |
| **Total Team Members**                      | **71**    | **100%**        |

##### Age Distribution

| **Age Distribution (Global)**               | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| 18-24                                       | 18        | 1.56%           |
| 25-29                                       | 224       | 19.38%          |
| 30-34                                       | 314       | 27.16%          |
| 35-39                                       | 247       | 21.37%          |
| 40-49                                       | 238       | 20.59%          |
| 50-59                                       | 103       | 8.91%           |
| 60+                                         | 11        | 0.95%           |
| Unreported                                  | 1         | 0.09%           |
| **Total Team Members**                      | **1,156** | **100%**        |


Source: GitLab's HRIS, BambooHR
